
<!--
Author:Charles Aondo
Date:2019-07-07
Purpose:This page is use to collect the new employee about a book that is to be added to the database using a form
and its requires a user logging before posting the form variables to the executeAddemployee for error handling and posting
into the database
-->
<!--Redirecting the user to the login page to login if the are not logged in-->


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Employee</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body class="backgroundDesign">
    <div id="Container">

        <form method="get" action="executeAddEmployee.php">

         <div class="form-group">
             <label>Employee ID</label>
             <input class="form-control" name="id" type="text" required>
         </div>
         <div class="form-group">
             <label>First Name</label>
             <input class="form-control" name="firstName" type="text" required>
         </div>
         <div class="form-group">
             <label>Last Name</label>
             <input class="form-control" name="lastName" type="text" required>
         </div>
         <div class="form-group">
             <label>Employee DOB</label>
             <input class="form-control" name="DOB" type="date" required>
         </div>

            <button type="submit" class="btn btn-primary" name="submit">Add Employee</button>
        </form>
    </div>
</body>
</html>
