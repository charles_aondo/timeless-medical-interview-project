<!--
Author:Charles Aondo
Date:2017-07-07
Purpose:This page to clean the data entered by the user, and post the data the collected from the edit page
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Updating Employee</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body id="loginBackground">
    <div id="Container">
        <?php

        if(!isset($_GET['submit'])){
            header("location:ViewEmployees.php");
            die("</body></html>");
        }
        //        Database Connection
        //        Page is setup this way to avoid using post for validation
        @$DB = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
        if(isset($_GET['submit'])){
            //Collecting and Santising the data before inserting into database
            $employeeID = mysqli_real_escape_string($DB,$_GET['id']);
            $firstName = mysqli_real_escape_string($DB,$_GET['firstName']);
            $lastName = mysqli_real_escape_string($DB,$_GET['lastName']);
            $DOB = mysqli_real_escape_string($DB,$_GET['dob']);
            $totalhours = mysqli_real_escape_string($DB,$_GET['totalHours']);
            $Dtrtype = mysqli_real_escape_string($DB,$_GET['dtrType']);

            //Validating the fields for empty data
            if(empty($firstName && $lastName && $DOB && $totalhours && $employeeID && $Dtrtype)){
                echo '<h2><a href="edit.php?isbn='.$employeeID.'" class="error">Empty field! Follow this link to go and fill the form</a></h2>';
            }else{
              //Inserting the collected data into the database
                $query = "UPDATE EmployeeMaster,DTRMaster SET EmployeeMaster.EmployeeLastName='$lastName',EmployeeMaster.EmployeeFirstName='$firstName',EmployeeMaster.EmployeeBday='$DOB',DTRMaster.DTRTotalHours='$totalhours',dtrmaster.DTRType='$Dtrtype'
                WHERE employeemaster.EmployeeID=dtrmaster.DTREmployeeID";
                //Executing the query
                $result = $DB->query($query);

                //Error validation check to see if the update was successful or not
                if($result->num_rows >0){
                    echo '<h2 class="passColor">Employee Updated Successfully</h2>'.'<br>';
                    echo '<h2><a href="addEmployee.php">Add Employee</a></h2>'.'<br>';
                    echo '<h2><a href="index.php">Home</a></h2>';
                }else{
                    echo'<h2 class="error">Failed to Update the Database!!! Please Contact IT Department</h2>'.'<br>';
                    echo '<h2><a class="passColor" href="edit.php?isbn='.$isbn.'">Click Here to Go Back</a></h2>'.'<br>';
                    echo '<h2><a href="index.php">Home</a></h2>';
                }
                //Free all database resources
                $result->free_result();
                $DB->close();
            }
        }
        ?>
    </div>
</body>
</html>