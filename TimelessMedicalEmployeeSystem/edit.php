
<!--
Author:Charles Aondo
Date:2019-07-07
Purpose:This page is use to edit an employee data
-->
<!--Redirecting the user to the login page to login if the are not logged in-->
<?php
    //Connecting to the database
    @$myDb = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
    //Getting the isbn from the url into a variable
    $employeeID = $_GET['EmployeeID'];
 //Redirect the user to the view page if the employee id is empty
    if(empty($employeeID)){
        header("location:ViewEmployees.php");
        die();
    }
    //Running mysql string to sanitized the data
    mysqli_real_escape_string($myDb,$employeeID);
    //https://stackoverflow.com/questions/27600017/delete-a-same-data-from-multiple-table-in-php
    $query ="SELECT a.*, b.* FROM EmployeeMaster a
                      INNER JOIN DTRMaster b ON a.EmployeeID = b.DTREmployeeID
                      WHERE a.EmployeeID='$employeeID'";
    $result = $myDb->query($query);
//Putting the query in an array
    $row= $result->fetch_assoc();

    //Putting the values in variables
    $employeeID = $row['EmployeeID'];
    $firstName = $row['EmployeeFirstName'];
    $lastName = $row['EmployeeLastName'];
    $DOB = $row['EmployeeBday'];
    $DTRType = $row['DTRType'];
    $totalhours = $row['DTRTotalHours'];

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit Employee</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body class="backgroundDesign">
    <div id="Container">

<!--        Prefill the form for the user to make it easy for them to edit an employee-->
        <form method="get" action="executeEditEmployee.php">

            <div class="form-group">
                <label disabled="">Employee ID</label>
                <input value="<?php echo $employeeID?>" class="form-control" disabled type="text">
                <input value="<?php echo $employeeID?>" class="form-control" name="id" type="hidden">
            </div>
            <div class="form-group">
                <label>First Name</label>
                <input value="<?php echo $firstName?>" class="form-control" name="firstName" type="text" required>
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input value="<?php echo $lastName?>"class="form-control" name="lastName" type="text" required>
            </div>
            <div class="form-group">
                <label>DOB</label>
                <input value="<?php echo $DOB?>" class="form-control" name="dob" type="date" required>
            </div>
            <div class="form-group">
                <label>Total Hours</label>
                <input value="<?php echo $totalhours?>" class="form-control" name="totalHours" type="number" required>
            </div>
            <div class="form-group">
                <label for="inputCity">DTR Type</label>
                <select name="dtrType" class="form-control">
                    <option value="Out">Out</option>
                    <option  value="In" selected>In</option>
                </select>
            </div>

            <button type="submit" class="btn btn-primary" name="submit">Update Employee</button>
        </form>
    </div>
</body>
</html>
