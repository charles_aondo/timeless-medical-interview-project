<!--
Author:Charles Aondo
Date:2018-12-03
Purpose:This is the main page for the entire system, this page provides the main functionality of the system
-->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Book List</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body class="backgroundDesign">
    <div id="Container">
<!--        Creating the two users of the system, the admin user and the main user-->
<!--        Click on admin to perform crude and employeee to login-->
        <form>
            <h3>Click on Admin to perform CRUDE functions and Employee to Login</h3>
            <input type="button"value="Admin" onclick="window.location.href='login.php'">
            <input type="button"value="Employee" onclick="window.location.href='employeeClockIn.php'">
        </form>
        <?php

        ?>
    </div>
</body>
</html>