 <!--Page checks if a user is logged in, if the are not, it redirects the user to the logging page -->
<?php
    session_start();
    if(!$_SESSION["isLoggedIn"]){
        header("location: login.php");
    }
?>