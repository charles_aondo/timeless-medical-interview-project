
<!--
Author:Charles Aondo
Date:2019-07-07
Purpose:This page is to allow an employee to login and the page will calculate the amount of time an employee works after a successful login
into the system
-->
<!--Redirecting the user to the login page to login if the are not logged in-->

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Employee Clock Out</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/myPageStylesheet.css">
    <script src="bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
</head>
<body class="backgroundDesign">
    <div id="Container">

        <form method="get" action="clockout.php">
             <div class="form-group">
        <label>Employee ID</label>
        <input class="form-control" name="id" type="text" required>
        <button type="submit" class="btn btn-primary" name="clockOut">Clock Out</button>
    </div>
    </form>
    <?php
    session_start();
    if(isset($_GET['clockOut'])){
        //Connecting to the database
        @$DB = new mysqli('localhost', 'root', '', 'timelessMedicalEmployee');
        //Checking for errors in database connection
        if(mysqli_connect_error()){
            echo '<h2 class="error">Database Error!! System is Down, Please Report to a Supervisor to Clock In!<a href="addEmployee.php"></a></h2>';
            die("</body></html>");
        }
        //Using mysql real escape string to escaped malicious user data
        $employeeID = mysqli_real_escape_string($DB,$_GET['id']);


//                //Validating the fields for empty data, it is a good practice not to rely on html required attribute for data validation your user data
        if(empty($employeeID)){
            echo '<h2><a href="executeEmployeeSign.php" class="error">Empty field! Follow this link to Sign In</a></h2>';
        }
        //Checking the db to see if the employee signing in is a valid employee with the company
        else{
            $query = "SELECT * FROM DTRMaster WHERE DTREmployeeID='$employeeID'";
            $result = mysqli_query($DB, $query);
            $count  = mysqli_num_rows($result);
        }
        //Employee has successfully signed in,time to start calculating hours
        if($count == 1){
            echo '<h2 class="passColor">Successfully Clocked Out, Have a great Day</h2>'.'<br>';
            unset($_SESSION['isLoggedInEmployee']);
        }else{
            echo '<h2>Fatal Error!! Retry or Contact a supervisor</h2>';
            echo '<h2><a href="employeeSignIn.php">Retry</a></h2>'.'<br>';
        }
    if(isset($_SESSION["isLoggedInEmployee"]) == false) {
        //Calculating the time in minutes since the user logged in.
        $timeSinceLogin =( time () - $_SESSION ['timer']) /3600;
        //Getting and setting the time zone to be inserted into the DB

        date_default_timezone_set("America/Halifax");
        $dateTime =  date("Y-m-d H:i:s");
        $status ="Out";
        //Updating the database with the time and date the user clocked in and the DTR type
        $stmt = $DB->prepare("UPDATE  DTRMaster SET DTRDateTime=?,DTRType = ?, DTRTotalHours = ? WHERE DTREmployeeID = ?");
        $stmt->bind_param("ssss",$dateTime,$status,$timeSinceLogin,$employeeID);
        $stmt->execute();
        header("location:index.php");
        die("</body></html>");
    }
        //Free and close all database resources

        $DB->close();

    }

    ?>
</div>

</body>